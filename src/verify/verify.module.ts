import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UserSchema } from 'src/Common/schema/user.schema';
import { VerifySchema } from 'src/Common/schema/verify.schema';
import { JWTService } from 'src/Common/Service/jwt.service';
import { MailService } from 'src/Common/Service/mail.service';
import { VerifyController } from './verify.controller';
import { VerifyService } from './verify.service';

@Module({
    controllers: [VerifyController],
    providers: [VerifyService, MailService, JWTService],
    imports: [
        MongooseModule.forFeature([{ name: 'Verify', schema: VerifySchema }]),
        MongooseModule.forFeature([{ name: 'User', schema: UserSchema }])
    ]
})
export class VerifyModule { }
