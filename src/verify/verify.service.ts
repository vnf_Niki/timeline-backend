import { ConflictException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateVerifyDTO, ResolveVerifyDTO } from 'src/Common/dto/verify.dto';
import { MailOptions } from 'src/Common/interface/mailOptions.interface';
import { User } from 'src/Common/interface/user.interface';
import { Verify } from 'src/Common/interface/verify.interface';
import { JWTService } from 'src/Common/Service/jwt.service';
import { MailService } from 'src/Common/Service/mail.service';
import { environment } from 'src/environments/environment';

@Injectable()
export class VerifyService {

    constructor(
        @InjectModel('Verify') private VerifyModel: Model<Verify>,
        @InjectModel('User') private UserModel: Model<User>,
        private readonly jwtService: JWTService,
        private mailService: MailService) { }

    async create(CreateVerifyDTO: CreateVerifyDTO, userToken): Promise<any> {

        try {
            let user = <User>await this.UserModel.findById(userToken.id).exec();

            CreateVerifyDTO.code = Math.random().toString(10).substring(2, 5) + Math.random().toString(10).substring(2, 5);
            CreateVerifyDTO.userRef = user.id;
            CreateVerifyDTO.iat = new Date(new Date().getTime() + (10 * 60 * 1000));

            let mO = {} as MailOptions
            mO.from = environment.mailVerify.from;
            mO.to = user.email;
            mO.subject = environment.mailVerify.subject;
            mO.text = environment.mailVerify.text + CreateVerifyDTO.code;

            <Verify>await this.VerifyModel.findOneAndDelete({ userRef: user.id });

            CreateVerifyDTO.userRef = user.id;
            const createdCat = new this.VerifyModel(CreateVerifyDTO);
            <Verify>await createdCat.save();

            return await this.mailService.sendOne(mO);;
        } catch (error) {
            throw new ConflictException(environment.tokenNotCreated);
        }
    }

    async resolve(ResolveVerifyDTO: ResolveVerifyDTO, userToken): Promise<any> {

        try {
            let user = <User>await this.UserModel.findById(userToken.id).exec();
            let verify = <Verify>await this.VerifyModel.findOne({ userRef: userToken.id + '', code: ResolveVerifyDTO.code });

            if ((!user || user === null) || (!verify || verify === null))
                throw new NotFoundException(environment.codeNotFound);

            <Verify>await this.VerifyModel.findByIdAndDelete(verify.id);
            user.emailVerified = true;
            return await this.UserModel.findByIdAndUpdate(userToken.id, user, { new: true });
        } catch (error) {
            throw new NotFoundException(environment.codeNotFound);
        }
    }
}
