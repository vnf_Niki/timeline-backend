import { Body, Controller, Headers, HttpStatus, Post, Res } from '@nestjs/common';
import { CreateVerifyDTO, ResolveVerifyDTO } from 'src/Common/dto/verify.dto';
import { JWTService } from 'src/Common/Service/jwt.service';
import { VerifyService } from './verify.service';

@Controller('verify')
export class VerifyController {

    constructor(private readonly VerifyService: VerifyService, private jwt: JWTService) { }

    @Post('/new')
    async createVerify(@Headers() headers, @Res() res, @Body() CreateVerifyDTO: CreateVerifyDTO) {
        const lists = await this.VerifyService.create(CreateVerifyDTO, await this.jwt.returnUserId(headers['auth-token']));
        return res.status(HttpStatus.OK).json({
            message: "Verify has been created successfully",
            lists
        })
    }

    @Post('/resolve')
    async resolveVerify(@Headers() headers, @Res() res, @Body() ResolveVerifyDTO: ResolveVerifyDTO) {
        const lists = await this.VerifyService.resolve(ResolveVerifyDTO, await this.jwt.returnUserId(headers['auth-token']));
        return res.status(HttpStatus.OK).json({
            message: "Verify has been resolved successfully",
            lists
        })
    }
}