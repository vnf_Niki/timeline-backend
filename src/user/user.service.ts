import { ConflictException, ForbiddenException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User } from '../Common/interface/user.interface';
import { CreateUserDTO, EditUserDTO, ForgotUserDTO, LoginUserDTO, ResetUserDTO } from '../Common/dto/user.dto';
import { environment } from 'src/environments/environment'
import { JwtService } from '@nestjs/jwt';

//Bcrypt Passwort hash
import { genSalt } from "bcrypt";
import { hash } from "bcrypt";
import { compare } from "bcrypt";
import { MailService } from 'src/Common/Service/mail.service';
import { MailOptions } from 'src/Common/interface/mailOptions.interface';
import { RecoverService } from 'src/Common/Service/recover.service';
import { CreateRecoverDTO } from 'src/Common/dto/recover.dto';
import { Recover } from 'src/Common/interface/recover.interface';
import { ImageService } from 'src/image/image.service';
import { InviteService } from 'src/invite/invite.service';

@Injectable()
export class UserService {

    constructor(
        @InjectModel('User') private UserModel: Model<User>,
        private readonly jwtService: JwtService,
        private mailService: MailService,
        private recoverService: RecoverService,
        private imageService: ImageService,
        private inviteService: InviteService) { }

    async register(CreateUserDTO: CreateUserDTO, originalname): Promise<any> {

        if (CreateUserDTO.inviteCode !== environment.adminCode)
            if (!await this.inviteService.use({ code: CreateUserDTO.inviteCode }))
                throw new ConflictException(environment.inviteNotFound);

        // Generate password hash
        const salt = await genSalt(environment.saltStrength);
        CreateUserDTO.password = await hash(CreateUserDTO.password, salt);
        CreateUserDTO.createdAt = new Date();
        CreateUserDTO.updatedAt = new Date();
        CreateUserDTO.emailVerified = false;

        const createdCat = new this.UserModel(CreateUserDTO);

        try {
            let user = <User>await createdCat.save();

            //renameImageTo Name-date.extention
            if (CreateUserDTO.image) {
                let newImage = await CreateUserDTO.image.replace(originalname.split('.').slice(0, -1).join('.'), user.id);
                try {
                    await this.imageService.renameImage(CreateUserDTO.image, newImage);
                } catch (error) {
                    console.error("Could not rename Image! " + CreateUserDTO.image);
                }
                user.image = newImage;
            }

            user = <User>await this.UserModel.findByIdAndUpdate(user.id, user, { new: true });
            return { name: user.name, email: user.email, image: user.image };
        } catch (error) {
            try {
                await this.imageService.deleteImage(CreateUserDTO.image);
            } catch (error) {
                console.error("Could not delete Image! " + CreateUserDTO.image);
            }
            throw new ConflictException(environment.emaliAlreadyTaken);
        }
    }

    async login(LoginUserDTO: LoginUserDTO): Promise<any> {
        try {
            let user = <User>await this.UserModel.findOne({ email: LoginUserDTO.email + '' }).exec();

            if (await compare(LoginUserDTO.password, user.password)) {
                return { name: user.name, email: user.email, authToken: this.jwtService.sign({ id: user.id, exp: Math.floor(Date.now() / 1000) + (60 * 20160), image: user.image }) };
            } else {
                throw new ForbiddenException(environment.userOrPassWordWrong);
            }
        } catch (error) {
            throw new NotFoundException(environment.emailNotFound);
        }
    }

    async getUser(userToken): Promise<any> {
        try {
            let user = <User>await this.UserModel.findById(userToken.id).exec();

            return { name: user.name, email: user.email, image: user.image, groupRefs: user.groupRefs, 'emailVerified': user.emailVerified, 'invitecode': this.inviteService.find(user.id) };
        } catch (error) {
            throw new NotFoundException(environment.emailNotFound);
        }
    }

    async edit(EditUserDTO: EditUserDTO, userToken, originalname): Promise<any> {
        try {
            let user = <User>await this.UserModel.findById(userToken.id).exec();

            if (EditUserDTO.password && EditUserDTO.newPassword)
                if (await compare(EditUserDTO.password, user.password)) {

                    if (EditUserDTO.newPassword) {
                        const salt = await genSalt(environment.saltStrength);
                        user.password = await hash(EditUserDTO.newPassword, salt);
                    }
                }

            if (EditUserDTO.newName)
                user.name = EditUserDTO.newName;

            if (EditUserDTO.image) {
                try {
                    //renameImageTo Name-date.extention
                    let newImage = EditUserDTO.image.replace(originalname.split('.').slice(0, -1).join('.'), user.id);
                    try {
                        await this.imageService.renameImage(EditUserDTO.image, newImage);
                    } catch (error) {
                        console.error("Could not rename Image! " + EditUserDTO.image);
                    }
                    EditUserDTO.image = newImage;
                    await this.imageService.deleteImage(user.image);
                    user.image = EditUserDTO.image;

                } catch (error) {
                    throw new ForbiddenException(environment.userOrPassWordWrong);
                }
            }

            if (EditUserDTO.newName || EditUserDTO.newPassword || EditUserDTO.image) {
                user.updatedAt = new Date();
                user = await this.UserModel.findByIdAndUpdate(userToken.id, user, { new: true });
                return { name: user.name, email: user.email, image: user.image, groupRefs: user.groupRefs };
            } else
                throw new NotFoundException(environment.emailNotFound);

        } catch (error) {
            throw new NotFoundException(environment.emailNotFound);
        }
    }

    async forgotPassword(ForgotUserDTO: ForgotUserDTO): Promise<any> {
        try {
            let user = <User>await this.UserModel.findOne({ email: ForgotUserDTO.email + '' }).exec();

            let random = Math.random().toString(10).substring(2, 5) + Math.random().toString(10).substring(2, 5);

            let mO = {} as MailOptions
            mO.from = environment.mailResetPassword.from;
            mO.to = user.email;
            mO.subject = environment.mailResetPassword.subject;
            mO.text = environment.mailResetPassword.text + random;

            let rDT = new CreateRecoverDTO();
            rDT.code = random;
            this.recoverService.saveCode(rDT)

            return await this.mailService.sendOne(mO);
        } catch (error) {
            throw new NotFoundException(environment.emailNotFound);
        }
    }

    async resetPassword(ResetUserDTO: ResetUserDTO): Promise<any> {
        try {

            let code = <Recover>await this.recoverService.checkCode({ code: ResetUserDTO.code });

            if (code.code === ResetUserDTO.code) {
                let user = <User>await this.UserModel.findOne({ email: ResetUserDTO.email + '' }).exec();

                const salt = await genSalt(environment.saltStrength);
                user.password = await hash(ResetUserDTO.password, salt);

                user = await this.UserModel.findByIdAndUpdate(user.id, user, { new: true });
                return { id: user.id, name: user.name, email: user.email };
            }

        } catch (error) {
            throw new NotFoundException(environment.emailNotFound);
        }
    }

    async updateUserGroup(id, groupId) {
        let user = <User>await this.UserModel.findById(id);
        user.updatedAt = new Date();
        user.groupRefs.push(groupId);
        return await this.UserModel.findByIdAndUpdate(id, user, { new: true });
    }

    async deleteUserGroup(id, groupId) {
        let user = <User>await this.UserModel.findById(id);
        user.updatedAt = new Date();
        user.groupRefs = user.groupRefs.splice(user.groupRefs.indexOf(groupId), 1);
        return await this.UserModel.findByIdAndUpdate(id, user, { new: true });
    }

    async updateUserPost(id, postId) {
        let user = <User>await this.UserModel.findById(id);
        user.updatedAt = new Date();
        user.postRefs.push(postId);
        return await this.UserModel.findByIdAndUpdate(id, user, { new: true });
    }

    async deleteUserPost(id, postId) {
        let user = <User>await this.UserModel.findById(id);
        user.updatedAt = new Date();
        user.postRefs = user.postRefs.splice(user.postRefs.indexOf(postId), 1);
        return await this.UserModel.findByIdAndUpdate(id, user, { new: true });
    }
}