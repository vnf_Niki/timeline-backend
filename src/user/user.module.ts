import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { MongooseModule } from '@nestjs/mongoose';
import { MailService } from 'src/Common/Service/mail.service';
import { RecoverService } from 'src/Common/Service/recover.service';
import { environment } from 'src/environments/environment';
import { RecoverSchema } from 'src/Common/schema/recover.schema';
import { UserSchema } from 'src/Common/schema/user.schema';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { JWTService } from 'src/Common/Service/jwt.service';
import { ImageService } from 'src/image/image.service';
import { InviteModule } from 'src/invite/invite.module';

@Module({
  controllers: [UserController],
  providers: [UserService, MailService, RecoverService, JWTService, ImageService],
  imports: [
    MongooseModule.forFeature([{ name: 'User', schema: UserSchema }]),
    MongooseModule.forFeature([{ name: 'Recover', schema: RecoverSchema }]),
    JwtModule.register({ secret: environment.jwtSecret }),
    InviteModule],
  exports: [
    UserService
  ]
})
export class UserModule { }
