import { Controller, Res, HttpStatus, Post, Body, UseInterceptors, UploadedFile, UseGuards, Headers, Get } from '@nestjs/common';
import { UserService } from './user.service';
import { CreateUserDTO, LoginUserDTO, EditUserDTO, ForgotUserDTO, ResetUserDTO } from '../Common/dto/user.dto';
import { profileImageUploadFileInterceptor } from 'src/Common/utils/file-upload.utils';
import { AuthGuard } from 'src/Common/Guard/auth.guard';
import { JWTService } from 'src/Common/Service/jwt.service';

@Controller('user')
export class UserController {

    constructor(private readonly UserService: UserService, private jwt: JWTService) { }

    @Post('/register')
    @UseInterceptors(profileImageUploadFileInterceptor)
    async addUser(@Res() res, @Body() CreateUserDTO: CreateUserDTO, @UploadedFile() file) {
        if (file)
            CreateUserDTO.image = file.destination + file.filename;
        const lists = await this.UserService.register(CreateUserDTO, file ? file.originalname : null);
        return res.status(HttpStatus.OK).json({
            message: "User has been created successfully",
            lists
        })
    }

    @Post('/login')
    async login(@Res() res, @Body() LoginUserDTO: LoginUserDTO) {
        const lists = await this.UserService.login(LoginUserDTO);
        return res.status(HttpStatus.OK).json({
            message: "Login successful",
            lists
        })
    }

    @Get('/')
    @UseGuards(AuthGuard)
    async getUser(@Headers() headers, @Res() res) {
        const lists = await this.UserService.getUser(await this.jwt.returnUserId(headers['auth-token']));
        return res.status(HttpStatus.OK).json({
            message: "Got user successful",
            lists
        })
    }

    @Post('/edit')
    @UseInterceptors(profileImageUploadFileInterceptor)
    @UseGuards(AuthGuard)
    async editUser(@Headers() headers, @Res() res, @Body() EditUserDTO: EditUserDTO, @UploadedFile() file) {
        if (file)
            EditUserDTO.image = file.destination + file.filename;
        const lists = await this.UserService.edit(EditUserDTO, await this.jwt.returnUserId(headers['auth-token']), file ? file.originalname : null);
        return res.status(HttpStatus.OK).json({
            message: "Edit successful",
            lists
        })
    }

    @Post('/forgotPassword')
    async forgotPassword(@Res() res, @Body() ForgotUserDTO: ForgotUserDTO) {
        const lists = await this.UserService.forgotPassword(ForgotUserDTO);
        return res.status(HttpStatus.OK).json({
            message: "Recover E-Mail send!",
            lists
        })
    }

    @Post('/resetPassword')
    async resetPassword(@Res() res, @Body() ResetUserDTO: ResetUserDTO) {
        const lists = await this.UserService.resetPassword(ResetUserDTO);
        return res.status(HttpStatus.OK).json({
            message: "Password recovered!",
            lists
        })
    }

    @Get('/verify')
    @UseGuards(AuthGuard)
    async verify(@Headers() headers, @Res() res) {
        return res.status(HttpStatus.OK).json({
            message: "Got user successful",
            lists: { verify: true }
        })
    }

    // @Get('all')
    // async findAll(@Res() res) {
    //     const lists = await this.UserService.findAll();
    //     return res.status(HttpStatus.OK).json(lists);
    // }

    // @Get('id')
    // async findById(@Res() res, @Query('id') id: string) {
    //     const lists = await this.UserService.findById(id);
    //     if (!lists) throw new NotFoundException('Id does not exist!');
    //     return res.status(HttpStatus.OK).json(lists);
    // }

    // @Put('/update')
    // async update(@Res() res, @Query('id') id: string, @Body() CreateUserDTO: CreateUserDTO) {
    //     const lists = await this.UserService.update(id, CreateUserDTO);
    //     if (!lists) throw new NotFoundException('Id does not exist!');
    //     return res.status(HttpStatus.OK).json({
    //         message: 'Post has been successfully updated',
    //         lists
    //     });
    // }

    // @Delete('/delete')
    // async delete(@Res() res, @Query('id') id: string) {
    //     const lists = await this.UserService.delete(id);
    //     if (!lists) throw new NotFoundException('Post does not exist');
    //     return res.status(HttpStatus.OK).json({
    //         message: 'Post has been deleted',
    //         lists
    //     })
    // }
}