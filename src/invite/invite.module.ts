import { Module } from '@nestjs/common';
import { InviteService } from './invite.service';
import { InviteController } from './invite.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { UserSchema } from 'src/Common/schema/user.schema';
import { InviteSchema } from 'src/Common/schema/invite.schema';
import { JWTService } from 'src/Common/Service/jwt.service';

@Module({
  providers: [InviteService, JWTService],
  controllers: [InviteController],
  imports: [
    MongooseModule.forFeature([{ name: 'Invite', schema: InviteSchema }]),
    MongooseModule.forFeature([{ name: 'User', schema: UserSchema }])],
  exports: [InviteService]
})
export class InviteModule { }
