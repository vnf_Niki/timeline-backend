import { Body, Controller, Headers, HttpStatus, Post, Res } from '@nestjs/common';
import { CreateInviteDTO } from 'src/Common/dto/invite.dto';
import { JWTService } from 'src/Common/Service/jwt.service';
import { InviteService } from './invite.service';

@Controller('invite')
export class InviteController {

    constructor(private readonly InviteService: InviteService, private jwt: JWTService) { }

    @Post('/new')
    async createInvite(@Headers() headers, @Res() res, @Body() CreateInviteDTO: CreateInviteDTO) {
        const lists = await this.InviteService.create(CreateInviteDTO, await this.jwt.returnUserId(headers['auth-token']));
        return res.status(HttpStatus.OK).json({
            message: "Verify has been created successfully",
            lists
        })
    }
}
