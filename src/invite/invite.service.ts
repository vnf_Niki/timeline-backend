import { ConflictException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateInviteDTO, UseInviteDTO } from 'src/Common/dto/invite.dto';
import { Invite } from 'src/Common/interface/invite.interface';
import { User } from 'src/Common/interface/user.interface';
import { JWTService } from 'src/Common/Service/jwt.service';
import { environment } from 'src/environments/environment';

@Injectable()
export class InviteService {

    constructor(
        @InjectModel('Invite') private InviteModel: Model<Invite>,
        @InjectModel('User') private UserModel: Model<User>,
        private readonly jwtService: JWTService) { }

    async create(CreateInviteDTO: CreateInviteDTO, userToken): Promise<any> {

        try {
            let user = <User>await this.UserModel.findById(userToken.id).exec();

            if (!user.emailVerified)
                throw new ConflictException(environment.emialNotVerified);

            let invite = <Invite>await this.InviteModel.findOne({ userRef: user.id });

            if (invite !== null)
                return invite;

            CreateInviteDTO.userRef = user.id;
            CreateInviteDTO.code = Math.random().toString(10).substring(2, 5) + Math.random().toString(10).substring(2, 5);
            CreateInviteDTO.createdAt = new Date();

            const createdCat = new this.InviteModel(CreateInviteDTO);
            let inivte = <Invite>await createdCat.save();

            return { code: inivte.code };
        } catch (error) {
            throw new ConflictException(environment.tokenNotCreated);
        }
    }

    async use(UseInviteDTO: UseInviteDTO): Promise<any> {

        try {
            let invite = <Invite>await this.InviteModel.findOne({ code: UseInviteDTO.code });

            if (!invite || invite === null)
                return false;

            <Invite>await this.InviteModel.findByIdAndDelete(invite.id);
            return true;
        } catch (error) {
            return false;
        }
    }

    async find(userId): Promise<any> {
        try {
            return await this.InviteModel.findOne({ userRef: userId });
        } catch (error) {
            throw new NotFoundException(environment.codeNotFound);
        }
    }
}
