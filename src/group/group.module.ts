import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { GroupSchema } from 'src/Common/schema/group.schema';
import { PostSchema } from 'src/Common/schema/post.schema';
import { UserSchema } from 'src/Common/schema/user.schema';
import { JWTService } from 'src/Common/Service/jwt.service';
import { InviteModule } from 'src/invite/invite.module';
import { UserModule } from 'src/user/user.module';
import { GroupController } from './group.controller';
import { GroupService } from './group.service';

@Module({
  controllers: [GroupController],
  providers: [GroupService, JWTService],
  imports: [
    MongooseModule.forFeature([{ name: 'Group', schema: GroupSchema }]),
    MongooseModule.forFeature([{ name: 'User', schema: UserSchema }]),
    MongooseModule.forFeature([{ name: 'Post', schema: PostSchema }]),
    UserModule,
    InviteModule
  ],
  exports: [
    GroupService
  ]
})
export class GroupModule { }
