import { ConflictException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { compare, genSalt, hash } from 'bcrypt';
import { Model } from 'mongoose';
import { CreateGroupDTO, EditGroupInfoDTO, EditGroupPasswordDTO, DeleteGroupDTO, LeaveGroupDTO, JoinGroupDTO } from 'src/Common/dto/group.dto';
import { Group } from 'src/Common/interface/group.interface';
import { Post } from 'src/Common/interface/post.interface';
import { User } from 'src/Common/interface/user.interface';
import { environment } from 'src/environments/environment';
import { InviteService } from 'src/invite/invite.service';
import { UserService } from 'src/user/user.service';

@Injectable()
export class GroupService {

    constructor(
        @InjectModel('Group') private GroupModel: Model<Group>,
        @InjectModel('User') private UserModel: Model<User>,
        @InjectModel('Post') private PostModel: Model<Post>,
        private userService: UserService,
        private inviteService: InviteService) { }

    async addGroup(CreateGroupDTO: CreateGroupDTO, userToken): Promise<any> {

        const salt = await genSalt(environment.saltStrength);

        CreateGroupDTO.password = await hash(CreateGroupDTO.password, salt);
        CreateGroupDTO.usersRefs = [userToken.id];
        CreateGroupDTO.createdAt = new Date();
        CreateGroupDTO.updatedAt = new Date();

        const createdCat = new this.GroupModel(CreateGroupDTO);

        try {
            let group = <Group>await createdCat.save();

            await this.userService.updateUserGroup(userToken.id, group.id);

            return { id: group.id };
        } catch (error) {
            throw new ConflictException(environment.groupAlreadyTaken);
        }
    }

    async getGroupPostCount(groupid): Promise<any> {
        try {
            let group = <Group>await this.GroupModel.findById(groupid).exec();

            let users = [];

            for await (const user of group.usersRefs) {
                let u = <User>await this.UserModel.findById(user);
                users.push({ name: u.name, image: u.image });
            }

            return {
                id: group.id, name: group.name, titlebar: group.titlebar,
                description: group.description, userRefsUsers: users,
                count: group.postRefs.length
            };

        } catch (error) {
            throw new NotFoundException(environment.groupNotFound);
        }
    }

    async getGroup(groupid): Promise<any> {
        try {
            let group = <Group>await this.GroupModel.findById(groupid).exec();

            let users = [];

            for await (const user of group.usersRefs) {
                let u = <User>await this.UserModel.findById(user);
                users.push({ userId: u.id, name: u.name, image: u.image });
            }

            group.postRefs = await this.sortPostsByDate(group.postRefs);

            return {
                id: group.id, name: group.name, titlebar: group.titlebar,
                description: group.description, userRefsUsers: users,
                posts: group.postRefs
            };

        } catch (error) {
            throw new NotFoundException(environment.groupNotFound);
        }
    }

    async sortPostsByDate(posts) {
        let sorted = [];
        for await (const post of posts) {
            sorted.push(await this.PostModel.findById(post).exec());
        }
        sorted.sort(function (a, b) {
            return new Date(a.date).getTime() - new Date(b.date).getTime()
        });
        let ret = [];
        for await (const post of sorted) {
            await ret.push(post._id);
        }
        return ret;
    }

    async getPostCount(groupid): Promise<any> {
        try {
            let group = <Group>await this.GroupModel.findById(groupid).exec();

            return { count: group.postRefs.length };

        } catch (error) {
            throw new NotFoundException(environment.groupNotFound);
        }
    }

    async editGroup(EditGroupInfoDTO: EditGroupInfoDTO): Promise<any> {
        try {
            let group = <Group>await this.GroupModel.findById(EditGroupInfoDTO.groupId).exec();

            if (await compare(EditGroupInfoDTO.password, group.password)) {
                if (EditGroupInfoDTO.description)
                    group.description = EditGroupInfoDTO.description;
                if (EditGroupInfoDTO.titlebar)
                    group.titlebar = EditGroupInfoDTO.titlebar;

                if (EditGroupInfoDTO.description || EditGroupInfoDTO.titlebar) {
                    group.updatedAt = new Date();
                    return await this.GroupModel.findByIdAndUpdate(EditGroupInfoDTO.groupId, group, { new: true });
                }
            }
            return null;
        } catch (error) {
            throw new NotFoundException(environment.groupNotFound);
        }
    }

    async changePassword(EditGroupPasswordDTO: EditGroupPasswordDTO): Promise<any> {
        try {
            let group = <Group>await this.GroupModel.findById(EditGroupPasswordDTO.groupId).exec();

            if (await compare(EditGroupPasswordDTO.password, group.password) && EditGroupPasswordDTO.newPassword) {
                const salt = await genSalt(environment.saltStrength);
                group.password = await hash(EditGroupPasswordDTO.newPassword, salt);
                group.updatedAt = new Date();

                await this.GroupModel.findByIdAndUpdate(EditGroupPasswordDTO.groupId, group, { new: true });
                return { passwordChanged: true };
            }
            return { passwordChanged: false };
        } catch (error) {
            throw new NotFoundException(environment.groupNotFound);
        }
    }

    async joinGroup(JoinGroupDTO: JoinGroupDTO, userToken): Promise<any> {
        try {
            let group = <Group>await this.GroupModel.findById(JoinGroupDTO.groupId).exec();
            let user = <User>await this.UserModel.findById(userToken.id);

            if (await compare(JoinGroupDTO.password, group.password)) {

                group.usersRefs.push(user.id);
                user.groupRefs.push(group.id);

                await this.GroupModel.findByIdAndUpdate(group.id, group, { new: true });
                user = await this.UserModel.findByIdAndUpdate(userToken.id, user, { new: true });

                return { name: user.name, email: user.email, image: user.image, groupRefs: user.groupRefs, 'emailVerified': user.emailVerified, 'invitecode': this.inviteService.find(user.id) };
            }

            return { groupJoined: false };
        } catch (error) {
            throw new NotFoundException(environment.groupNotFound);
        }
    }

    async leaveGroup(LeaveGroupDTO: LeaveGroupDTO, userToken): Promise<any> {
        try {
            let group = <Group>await this.GroupModel.findById(LeaveGroupDTO.groupId).exec();
            let user = <User>await this.UserModel.findById(userToken.id);

            const index = group.usersRefs.indexOf(userToken.id);
            if (index > -1) {
                group.usersRefs.splice(index, 1);
            }

            const indexB = user.groupRefs.indexOf(group.id);
            if (index > -1) {
                user.groupRefs.splice(indexB, 1);
            }

            await this.GroupModel.findByIdAndUpdate(LeaveGroupDTO.groupId, group, { new: true });
            await this.UserModel.findByIdAndUpdate(userToken.id, user, { new: true });

            return { name: user.name, email: user.email, image: user.image, groupRefs: user.groupRefs, 'emailVerified': user.emailVerified, 'invitecode': this.inviteService.find(user.id) };
        } catch (error) {
            throw new NotFoundException(environment.groupNotFound);
        }
    }

    async deleteGroup(DeleteGroupDTO: DeleteGroupDTO, userToken): Promise<any> {
        try {
            let group = <Group>await this.GroupModel.findById(DeleteGroupDTO.groupId).exec();
            if (await compare(DeleteGroupDTO.password, group.password)) {
                await this.GroupModel.findByIdAndDelete(DeleteGroupDTO.groupId);
                await this.userService.deleteUserGroup(userToken.id, group.id);
                return { deletedGroup: group.id };
            }
            return { deletedGroup: null };
        } catch (error) {
            throw new NotFoundException(environment.groupNotFound);
        }
    }

    async updateGroupPost(id, postId) {
        let group = <Group>await this.GroupModel.findById(id);
        group.updatedAt = new Date();
        group.postRefs.push(postId);
        return await this.GroupModel.findByIdAndUpdate(id, group, { new: true });
    }

    async deleteGroupPost(id, postId) {
        let group = <Group>await this.GroupModel.findById(id);
        group.updatedAt = new Date();
        group.postRefs = group.postRefs.splice(group.postRefs.indexOf(postId), 1);
        return await this.GroupModel.findByIdAndUpdate(id, group, { new: true });
    }
}
