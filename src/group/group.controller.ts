import { Body, Controller, Get, HttpStatus, Post, Res, Query, Headers, UseGuards } from '@nestjs/common';
import { AuthGuard } from 'src/Common/Guard/auth.guard';
import { CreateGroupDTO, DeleteGroupDTO, EditGroupInfoDTO, EditGroupPasswordDTO, JoinGroupDTO, LeaveGroupDTO } from 'src/Common/dto/group.dto';
import { JWTService } from 'src/Common/Service/jwt.service';
import { GroupGuard } from 'src/Common/Guard/group.guard';
import { GroupService } from './group.service';

@Controller('group')
export class GroupController {

    constructor(private readonly GroupService: GroupService, private jwt: JWTService) { }

    @Post('/create')
    @UseGuards(AuthGuard)
    async addGroup(@Headers() headers, @Res() res, @Body() CreateGroupDTO: CreateGroupDTO) {
        const lists = await this.GroupService.addGroup(CreateGroupDTO, await this.jwt.returnUserId(headers['auth-token']));
        return res.status(HttpStatus.OK).json({
            message: "Group has been created successfully",
            lists
        })
    }

    @Get('/')
    @UseGuards(AuthGuard, GroupGuard)
    async getGroup(@Headers() headers, @Res() res, @Query('groupId') groupId: string) {

        const lists = await this.GroupService.getGroup(groupId);
        return res.status(HttpStatus.OK).json(lists);
    }

    @Get('/home/')
    @UseGuards(AuthGuard, GroupGuard)
    async getGroupPostCount(@Headers() headers, @Res() res, @Query('groupId') groupId: string) {

        const lists = await this.GroupService.getGroupPostCount(groupId);
        return res.status(HttpStatus.OK).json(lists);
    }

    @Get('/postCount')
    @UseGuards(AuthGuard, GroupGuard)
    async getPostCount(@Headers() headers, @Res() res, @Query('groupId') groupId: string) {

        const lists = await this.GroupService.getPostCount(groupId);
        return res.status(HttpStatus.OK).json(lists);
    }

    @Post('/edit')
    @UseGuards(AuthGuard, GroupGuard)
    async editGroup(@Res() res, @Body() EditGroupInfoDTO: EditGroupInfoDTO) {
        const lists = await this.GroupService.editGroup(EditGroupInfoDTO,);
        return res.status(HttpStatus.OK).json({
            message: "Group has been edited successfully",
            lists
        })
    }

    @Post('/changePassword')
    @UseGuards(AuthGuard, GroupGuard)
    async changePassword(@Res() res, @Body() EditGroupPasswordDTO: EditGroupPasswordDTO) {
        const lists = await this.GroupService.changePassword(EditGroupPasswordDTO);
        return res.status(HttpStatus.OK).json({
            message: "GroupPassword has been changed successfully",
            lists
        })
    }

    @Post('/join')
    @UseGuards(AuthGuard)
    async joinGroup(@Headers() headers, @Res() res, @Body() JoinGroupDTO: JoinGroupDTO) {
        const lists = await this.GroupService.joinGroup(JoinGroupDTO, await this.jwt.returnUserId(headers['auth-token']));
        return res.status(HttpStatus.OK).json({
            message: "Group has been joined successfully",
            lists
        })
    }

    @Post('/leave')
    @UseGuards(AuthGuard, GroupGuard)
    async leaveGroup(@Headers() headers, @Res() res, @Body() LeaveGroupDTO: LeaveGroupDTO) {
        const lists = await this.GroupService.leaveGroup(LeaveGroupDTO, await this.jwt.returnUserId(headers['auth-token']));
        return res.status(HttpStatus.OK).json({
            message: "Group has been left successfully",
            lists
        })
    }

    @Post('/delete')
    @UseGuards(AuthGuard, GroupGuard)
    async deleteGroup(@Headers() headers, @Res() res, @Body() DeleteGroupDTO: DeleteGroupDTO) {
        const lists = await this.GroupService.deleteGroup(DeleteGroupDTO, await this.jwt.returnUserId(headers['auth-token']));
        return res.status(HttpStatus.OK).json({
            message: "Group has been deleted successfully",
            lists
        })
    }
}
