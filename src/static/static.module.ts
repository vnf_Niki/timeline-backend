import { Module } from '@nestjs/common';
import { StaticService } from './static.service';
import { StaticController } from './static.controller';
import { UserService } from 'src/user/user.service';
import { JWTService } from 'src/Common/Service/jwt.service';
import { UserSchema } from 'src/Common/schema/user.schema';
import { MongooseModule } from '@nestjs/mongoose';
import { GroupSchema } from 'src/Common/schema/group.schema';
import { UserModule } from 'src/user/user.module';
import { GroupModule } from 'src/group/group.module';
import { PostSchema } from 'src/Common/schema/post.schema';
import { PostModule } from 'src/post/post.module';

@Module({
  controllers: [StaticController],
  providers: [StaticService, JWTService],
  imports: [
    MongooseModule.forFeature([{ name: 'User', schema: UserSchema }]),
    MongooseModule.forFeature([{ name: 'Group', schema: GroupSchema }]),
    MongooseModule.forFeature([{ name: 'Post', schema: PostSchema }]),
    UserModule,
    GroupModule,
    PostModule
  ]
})
export class StaticModule { }
