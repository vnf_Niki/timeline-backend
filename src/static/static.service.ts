import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User } from 'src/Common/interface/user.interface';
import fs = require('fs');
import { Post } from 'src/Common/interface/post.interface';
import { GetStaticImageDTO } from 'src/Common/dto/static.dto';

@Injectable()
export class StaticService {

    constructor(
        @InjectModel('User') private UserModel: Model<User>,
        @InjectModel('Post') private PostModel: Model<Post>) { }

    async getMeImage(userToken): Promise<any> {
        try {
            let user = <User>await this.UserModel.findById(userToken.id).exec();
            return await this.base64_encode(user.image);
        } catch (error) {
            console.error('Could not read Image! ' + userToken.id)
        }
    }

    async getUserImage(userId): Promise<any> {
        try {
            let user = <User>await this.UserModel.findById(userId).exec();
            return await this.base64_encode(user.image);
        } catch (error) {
            console.error('Could not read Image! ' + userId)
        }
    }

    async getImage(GetStaticImageDTO: GetStaticImageDTO): Promise<any> {
        try {
            return await this.base64_encode(GetStaticImageDTO.imagePath);
        } catch (error) {
            console.error('Could not read Image! ' + GetStaticImageDTO.imagePath)
        }
    }

    async base64_encode(file) {
        // read binary data
        var bitmap = await fs.readFileSync(file);
        // convert binary data to base64 encoded string
        return await new Buffer(bitmap).toString('base64');
    }
}
