import { Body, Controller, Get, Header, Headers, HttpStatus, Post, Query, Res, UseGuards } from '@nestjs/common';
import { GetStaticImageDTO } from 'src/Common/dto/static.dto';
import { AuthGuard } from 'src/Common/Guard/auth.guard';
import { GroupGuard } from 'src/Common/Guard/group.guard';
import { JWTService } from 'src/Common/Service/jwt.service';
import { StaticService } from './static.service';

@Controller('static')
export class StaticController {

    constructor(private readonly StaticService: StaticService, private jwt: JWTService) { }

    @Get('/image/me')
    @UseGuards(AuthGuard)
    async getMeImage(@Headers() headers, @Res() res) {
        try {
            let data = await this.StaticService.getMeImage(await this.jwt.returnUserId(headers['auth-token']));
            return await res.status(HttpStatus.OK).end(data);
        } catch (error) {
            return res.status(HttpStatus.NOT_FOUND).send();
        }
    }

    @Get('/image/user')
    @UseGuards(AuthGuard)
    async getUserImage(@Headers() headers, @Res() res, @Query('userId') userId: string) {
        try {
            let data = await this.StaticService.getUserImage(userId);
            return await res.status(HttpStatus.OK).end(data);
        } catch (error) {
            return res.status(HttpStatus.NOT_FOUND).send();
        }
    }

    @Post('/image')
    @UseGuards(AuthGuard, GroupGuard)
    async getImage(@Headers() headers, @Res() res, @Body() GetStaticImageDTO: GetStaticImageDTO) {
        try {
            let data = await this.StaticService.getImage(GetStaticImageDTO);
            return await res.status(HttpStatus.OK).end(data);
        } catch (error) {
            return res.status(HttpStatus.NOT_FOUND).send();
        }
    }
}
