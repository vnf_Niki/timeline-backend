import { Body, Controller, Headers, HttpStatus, Post, Req, Res, Query, UploadedFile, UploadedFiles, UseGuards, UseInterceptors } from '@nestjs/common';
import { AuthGuard } from 'src/Common/Guard/auth.guard';
import { CreatePostDTO, DeletePostDTO, EditPostInfoDTO, GetPostDTO } from 'src/Common/dto/post.dto';
import { JWTService } from 'src/Common/Service/jwt.service';
import { GroupGuard } from 'src/Common/Guard/group.guard';
import { PostGuard } from 'src/Common/Guard/post.guard';
import { PostService } from './post.service';
import { postImagesUploadFileInterceptor, postImageUploadFileInterceptor } from 'src/Common/utils/file-upload.utils';

@Controller('post')
export class PostController {
    constructor(private readonly PostService: PostService, private jwt: JWTService) { }

    @Post('/create')
    @UseGuards(AuthGuard, GroupGuard)
    async addPost(@Headers() headers, @Res() res, @Body() CreatePostDTO: CreatePostDTO) {

        const lists = await this.PostService.addPost(CreatePostDTO, await this.jwt.returnUserId(headers['auth-token']));
        return res.status(HttpStatus.OK).json({
            message: "Post has been created successfully",
            lists
        })
    }

    @Post('/createImages')
    @UseInterceptors(postImagesUploadFileInterceptor)
    @UseGuards(AuthGuard, GroupGuard)
    async addPostImages(@Query('groupId') groupId: string, @Res() res, @UploadedFiles() files) {

        let list = [];
        files.forEach(file => {
            list.push(file.destination + file.filename);
        });

        const lists = { list };
        return res.status(HttpStatus.OK).json({
            message: "Post has been created successfully",
            lists
        })
    }

    @Post('/getPosts')
    @UseGuards(AuthGuard, GroupGuard, PostGuard)
    async getPosts(@Headers() headers, @Res() res, @Body() GetPostDTO: GetPostDTO) {

        const lists = await this.PostService.getPosts(GetPostDTO);
        return res.status(HttpStatus.OK).json(lists);
    }

    @Post('/edit')
    @UseInterceptors(postImageUploadFileInterceptor)
    @UseGuards(AuthGuard, GroupGuard, PostGuard)
    async editPosts(@Headers() headers, @Res() res, @Body() EditPostInfoDTO: EditPostInfoDTO, @UploadedFiles() files) {
        let oldFilenames = null;
        if (files) {
            let filenames = [];
            oldFilenames = [];
            files.forEach(file => {
                filenames.push(file.destination + file.filename)
                oldFilenames.push(file.originalname);
            });
            EditPostInfoDTO.images = filenames;
        }
        const lists = await this.PostService.editPost(EditPostInfoDTO, await this.jwt.returnUserId(headers['auth-token']), oldFilenames);
        return res.status(HttpStatus.OK).json({
            message: "Post has been edited successfully",
            lists
        })
    }

    @Post('/delete')
    @UseGuards(AuthGuard, GroupGuard, PostGuard)
    async deletePost(@Headers() headers, @Res() res, @Body() DeletePostDTO: DeletePostDTO) {
        const lists = await this.PostService.deletePost(DeletePostDTO);
        return res.status(HttpStatus.OK).json({
            message: "Post has been deleted successfully",
            lists
        })
    }
}