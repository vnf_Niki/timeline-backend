import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { PostController } from './post.controller';
import { PostService } from './post.service';
import { PostSchema } from 'src/Common/schema/post.schema';
import { UserModule } from 'src/user/user.module';
import { JWTService } from 'src/Common/Service/jwt.service';
import { GroupSchema } from 'src/Common/schema/group.schema';
import { ImageService } from 'src/image/image.service';
import { GroupModule } from 'src/group/group.module';

@Module({
    controllers: [PostController],
    providers: [PostService, JWTService, ImageService],
    imports: [
        MongooseModule.forFeature([{ name: 'Post', schema: PostSchema }]),
        MongooseModule.forFeature([{ name: 'Group', schema: GroupSchema }]),
        UserModule,
        GroupModule
    ]
})
export class PostModule { }
