import { ConflictException, ForbiddenException, Injectable, InternalServerErrorException, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreatePostDTO, DeletePostDTO, EditPostInfoDTO, GetPostDTO } from 'src/Common/dto/post.dto';
import { Post } from 'src/Common/interface/post.interface';
import { environment } from 'src/environments/environment';
import { GroupService } from 'src/group/group.service';
import { ImageService } from 'src/image/image.service';
import { UserService } from 'src/user/user.service';

@Injectable()
export class PostService {

    constructor(
        @InjectModel('Post') private PostModel: Model<Post>,
        private userService: UserService,
        private groupService: GroupService,
        private imageService: ImageService) { }

    async addPost(CreatePostDTO: CreatePostDTO, userToken): Promise<any> {

        CreatePostDTO.userRef = userToken.id;
        CreatePostDTO.createdAt = new Date();
        CreatePostDTO.updatedAt = new Date();

        console.log(CreatePostDTO);

        if (!CreatePostDTO.titel)
            throw new InternalServerErrorException(environment.postNotCreated + 'titel');
        if (!CreatePostDTO.date)
            throw new InternalServerErrorException(environment.postNotCreated + 'date');
        if (!CreatePostDTO.description)
            throw new InternalServerErrorException(environment.postNotCreated + 'desc');
        if (CreatePostDTO.nsfw === undefined)
            throw new InternalServerErrorException(environment.postNotCreated + 'nsfw');
        if (!CreatePostDTO.images)
            throw new InternalServerErrorException(environment.postNotCreated + 'images');

        const createdCat = new this.PostModel(CreatePostDTO);

        try {
            let post = <Post>await createdCat.save();
            await this.userService.updateUserPost(userToken.id, post.id);
            await this.groupService.updateGroupPost(CreatePostDTO.groupId, post.id);

            return <Post>await this.PostModel.findByIdAndUpdate(post.id, post, { new: true });
        } catch (error) {
            throw new InternalServerErrorException(environment.postNotCreated);
        }
    }

    async getPosts(GetPostDTO: GetPostDTO): Promise<any> {
        try {
            let posts = [];
            for await (const id of GetPostDTO.postIds) {
                try {
                    let post = <Post>await this.PostModel.findById(id).exec();
                    posts.push(post);
                } catch (error) { }
            }
            return posts;
        } catch (error) {
            throw new NotFoundException(environment.postNotFound);
        }
    }

    async editPost(EditPostInfoDTO: EditPostInfoDTO, userToken, oldFilenames): Promise<any> {
        try {
            let post = <Post>await this.PostModel.findById(EditPostInfoDTO.postId).exec();

            if (EditPostInfoDTO.titel)
                post.titel = EditPostInfoDTO.titel;
            if (EditPostInfoDTO.description)
                post.description = EditPostInfoDTO.description;
            if (EditPostInfoDTO.date)
                post.date = EditPostInfoDTO.date;
            if (EditPostInfoDTO.images) {
                try {
                    //renameImageTo Name-date.extention
                    let newImages = [];
                    for (let i = 0; i < oldFilenames.length; i++) {
                        newImages.push(await EditPostInfoDTO.images[i].replace(oldFilenames[i].split('.').slice(0, -1).join('.'), post.id + '_' + i));
                    }
                    try {
                        await this.imageService.renameImages(EditPostInfoDTO.images, newImages);
                    } catch (error) {
                        console.error("Could not rename Image! " + EditPostInfoDTO.images);
                    }
                    post.images = newImages;
                    await this.imageService.deleteImages(post.images);
                } catch (error) {
                    console.error("Could not delete Image! " + post.images);
                }
                post.images = EditPostInfoDTO.images;
            }
            if (EditPostInfoDTO.nsfw)
                post.nsfw = EditPostInfoDTO.nsfw;

            if (EditPostInfoDTO.titel || EditPostInfoDTO.description || EditPostInfoDTO.date || EditPostInfoDTO.images || EditPostInfoDTO.nsfw) {
                post.updatedAt = new Date();
                return await this.PostModel.findByIdAndUpdate(EditPostInfoDTO.groupId, post, { new: true });
            }
            return null;
        } catch (error) {
            throw new NotFoundException(environment.groupNotFound);
        }
    }

    async deletePost(DeletePostDTO: DeletePostDTO): Promise<any> {
        try {
            let post = <Post>await this.PostModel.findById(DeletePostDTO.postId).exec();
            await this.PostModel.findByIdAndDelete(DeletePostDTO.postId);
            await this.userService.deleteUserPost(post.userRef, post.id);
            return { deletedPost: post.id };
        } catch (error) {
            throw new NotFoundException(environment.groupNotFound);
        }
    }
}
