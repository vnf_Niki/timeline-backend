import { Injectable } from '@nestjs/common';
import fs = require('fs');

@Injectable()
export class ImageService {

    async renameImages(images, newImages) {
        for (let i = 0; i < images.length; i++) {
            await fs.renameSync(images[i], newImages[i]);
        }
        return true;
     }

    async renameImage(image, newImage) {
       return await fs.renameSync(image, newImage);
    }

    async deleteImages(images) {
        for (let i = 0; i < images.length; i++) {
            await await fs.unlinkSync(images[i]);
        }
        return true;
    }

    async deleteImage(image) {
        return await fs.unlinkSync(image);
    }
}
