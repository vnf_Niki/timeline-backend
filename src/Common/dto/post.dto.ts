import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsDate, IsNotEmpty, Length } from 'class-validator';

export class CreatePostDTO {

    userRef: string;

    @ApiProperty()
    @IsNotEmpty()
    @Length(5, 20)
    titel: string;

    @ApiProperty()
    @IsNotEmpty()
    @IsDate()
    date: Date;

    @ApiProperty()
    @IsNotEmpty()
    description: string;

    @ApiProperty()
    @IsNotEmpty()
    images: string[];

    @ApiProperty()
    @IsNotEmpty()
    @IsBoolean()
    nsfw: boolean;

    @ApiProperty()
    @IsNotEmpty()
    groupId: string;

    createdAt: Date;
    updatedAt: Date;
}

export class GetPostDTO {

    @ApiProperty()
    @IsNotEmpty()
    groupId: string;

    @ApiProperty()
    @IsNotEmpty()
    postIds: string[];
}

export class EditPostInfoDTO {

    @ApiProperty()
    @IsNotEmpty()
    groupId: string;

    @ApiProperty()
    @IsNotEmpty()
    postId: string

    @ApiProperty()
    titel: string;

    @ApiProperty()
    date: Date;

    @ApiProperty()
    description: string;

    images: string[];

    @ApiProperty()
    nsfw: boolean;
}

export class DeletePostDTO {

    @ApiProperty()
    @IsNotEmpty()
    groupId: string;

    @ApiProperty()
    @IsNotEmpty()
    postId: string;
}