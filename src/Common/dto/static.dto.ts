import { ApiProperty } from '@nestjs/swagger';

export class GetStaticImageDTO {

    groupId: string;
    imagePath: string;
}