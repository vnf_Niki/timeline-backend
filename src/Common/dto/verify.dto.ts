import { ApiProperty } from '@nestjs/swagger';

export class CreateVerifyDTO {

    code: string;
    userRef: string;
    iat: Date;
}

export class ResolveVerifyDTO {

    @ApiProperty()
    code: string;
}
