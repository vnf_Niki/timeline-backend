import { ApiProperty } from '@nestjs/swagger';

export class CreateInviteDTO {

    code: string;
    userRef: string;
    createdAt: Date;
}

export class UseInviteDTO {

    @ApiProperty()
    code: string;
}
