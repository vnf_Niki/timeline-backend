import { ApiProperty } from '@nestjs/swagger';
import { buildMessage, IsNotEmpty } from 'class-validator';

export class CreateGroupDTO {

    @ApiProperty()
    @IsNotEmpty()
    name: string;

    @ApiProperty()
    @IsNotEmpty()
    description: string;

    @ApiProperty()
    @IsNotEmpty()
    password: string;

    usersRefs: string[];
    createdAt: Date;
    updatedAt: Date;
}

export class GetGroupInfoDTO {

    @ApiProperty()
    @IsNotEmpty()
    groupId: string;

}

export class EditGroupInfoDTO {

    @ApiProperty()
    @IsNotEmpty()
    groupId: string;

    @ApiProperty()
    @IsNotEmpty()
    password: string;

    @ApiProperty()
    @IsNotEmpty()
    description: string;

    @ApiProperty()
    @IsNotEmpty()
    titlebar: [];
}

export class EditGroupPasswordDTO {

    @ApiProperty()
    groupId: string;

    @ApiProperty()
    password: string;

    @ApiProperty()
    newPassword: string;
}

export class JoinGroupDTO {

    @ApiProperty()
    @IsNotEmpty()
    groupId: string;

    @ApiProperty()
    @IsNotEmpty()
    password: string;
}

export class LeaveGroupDTO {

    @ApiProperty()
    @IsNotEmpty()
    groupId: string;
}

export class DeleteGroupDTO {

    @ApiProperty()
    @IsNotEmpty()
    groupId: string;

    @ApiProperty()
    @IsNotEmpty()
    password: string;
}