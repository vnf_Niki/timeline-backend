import { ApiProperty } from '@nestjs/swagger';

export class CreateRecoverDTO {

    @ApiProperty()
    code: string;

    iat = new Date(new Date().getTime() + (10 * 60 * 1000));
}