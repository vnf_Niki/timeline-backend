import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, Length } from 'class-validator';

export class CreateUserDTO {

    @ApiProperty()
    @IsNotEmpty()
    @Length(4, 20)
    name: string;

    @ApiProperty()
    @IsNotEmpty()
    @IsEmail()
    email: string;

    @ApiProperty()
    @IsNotEmpty()
    @Length(5, 20)
    password: string;

    @ApiProperty()
    @IsNotEmpty()
    inviteCode: string;

    image: string;

    emailVerified: boolean;

    createdAt: Date;
    updatedAt: Date;
}

export class LoginUserDTO {

    @ApiProperty()
    @IsNotEmpty()
    email: string;

    @ApiProperty()
    @IsNotEmpty()
    password: string;
}

export class EditUserDTO {

    @ApiProperty()
    newName: string;

    @ApiProperty()
    password: string;

    @ApiProperty()
    newPassword: string;

    image: string;
}

export class ForgotUserDTO {

    @ApiProperty()
    @IsNotEmpty()
    email: string;
}

export class ResetUserDTO {

    @ApiProperty()
    @IsNotEmpty()
    email: string;

    @ApiProperty()
    @IsNotEmpty()
    code: string;

    @ApiProperty()
    @IsNotEmpty()
    @Length(5, 20)
    password: string;
}