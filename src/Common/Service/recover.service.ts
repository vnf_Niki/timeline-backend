import { ConflictException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Recover } from '../interface/recover.interface';
import { CreateRecoverDTO } from '../dto/recover.dto';
import { environment } from '../../environments/environment'

@Injectable()
export class RecoverService {

    constructor(@InjectModel('Recover') private RecoverModel: Model<Recover>) { }

    async saveCode(CreateRecoverDTO: CreateRecoverDTO): Promise<any> {

        const createdCat = new this.RecoverModel(CreateRecoverDTO);

        try {
            let recover = <Recover>await createdCat.save();

            return { code: recover.code };
        } catch (error) {
            throw new ConflictException();
        }
    }

    async checkCode(CodeData: { code: String }): Promise<any> {
        try {
            let recover = <Recover>await this.RecoverModel.findOne({ code: CodeData.code + '' }).exec();

            await this.RecoverModel.findByIdAndRemove(recover.id);

            if (recover.iat > new Date())
                return {code: recover.code};
            else
                throw new NotFoundException(environment.codeExpired);


        } catch (error) {
            throw new NotFoundException(environment.codeExpired);
        }

    }
}