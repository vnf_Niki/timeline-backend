import { Injectable } from '@nestjs/common';
import { verify } from 'jsonwebtoken';
import { environment } from 'src/environments/environment';

@Injectable()
export class JWTService {

    constructor() {}

    async verifyToken(token): Promise<boolean>{
        try {
            await verify(token, environment.jwtSecret)
            return true;
        } catch (error) {
            return false;
        }
    }

    async returnUserId(token): Promise<{}> {
        const base64Url = token.split('.')[1];
        const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
        const buff = Buffer.from(base64, 'base64');
        const payloadinit = buff.toString('ascii');
        return Promise.resolve(await JSON.parse(payloadinit));
    };
}

