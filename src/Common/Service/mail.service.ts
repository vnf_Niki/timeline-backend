import { Injectable, ServiceUnavailableException } from "@nestjs/common";
import { resolve } from "path";
import { environment } from "src/environments/environment";
import { MailOptions } from "src/Common/interface/mailOptions.interface";

var nodemailer = require('nodemailer');

// Mail for new password
var transporter = nodemailer.createTransport({
    pool: true,
    host: environment.mailHost,
    secure: true,
    auth: environment.mailAuth
});

@Injectable()
export class MailService {

    constructor() { }

    async sendOne(mailOptions: MailOptions): Promise<any> {

        try {
            await transporter.sendMail(mailOptions);
            return true;
        } catch (error) {
            throw new ServiceUnavailableException(environment.emailNotSend);
        }
    }
}