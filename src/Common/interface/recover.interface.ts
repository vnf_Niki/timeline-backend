import { Document } from 'mongoose';

export interface Recover extends Document {

    code: string,
    readonly iat: Date;
}