import { Document } from 'mongoose';

export interface Post extends Document {

    titel: string;
    date: Date;
    userRef: string;
    description: string;
    images: string[];
    nsfw: boolean;
    groupRef: string;
    createdAt: Date;
    updatedAt: Date;
}