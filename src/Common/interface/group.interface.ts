import { Document } from 'mongoose';

export interface Group extends Document {

    name: string;
    password: string;
    description: string;
    titlebar: [];
    usersRefs: string[];
    postRefs: string[];
    createdAt: Date;
    updatedAt: Date;
}