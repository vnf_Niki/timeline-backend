import { Document } from 'mongoose';

export interface Invite extends Document {

    code: string,
    userRef: string,
    readonly createdAt: Date;
}