import { Document } from 'mongoose';

export interface User extends Document {

    name: string;
    password: string
    email: string;
    image: string;
    groupRefs: string[];
    postRefs: string[];
    emailVerified: boolean;

    createdAt: Date;
    updatedAt: Date;
}