import { Document } from 'mongoose';

export interface MailOptions extends Document {

    from: String;
    to: String;
    subject: String;
    text: String;
}