import { Document } from 'mongoose';

export interface Verify extends Document {

    code: string,
    userRef: string,
    readonly iat: Date;
}