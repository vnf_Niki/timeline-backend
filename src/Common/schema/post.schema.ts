import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

export const PostSchema = new mongoose.Schema({
    titel: { type: String, required: true, unique: true },
    date: { type: String, required: true },
    userRef: { type: String, required: true },
    description: { type: String, required: true },
    images: { type: [String] },
    nsfw: { type: Boolean, required: true },
    groupId: { type: String, required: true },
    createdAt: { type: Date, required: true },
    updatedAt: { type: Date, required: true }
});