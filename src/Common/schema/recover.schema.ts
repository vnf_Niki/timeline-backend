import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

export const RecoverSchema = new mongoose.Schema({
    code: {type: String, required : true},
    iat: {type: Date, required: true}
});