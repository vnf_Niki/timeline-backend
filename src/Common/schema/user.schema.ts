import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

export const UserSchema = new mongoose.Schema({
    name: { type: String, required: true },
    email: { type: String, unique: true, required: true },
    password: { type: String, required: true },
    image: { type: String },
    groupRefs: { type: [String] },
    postRefs: { type: [String] },
    emailVerified: { type: Boolean, required: true },

    createdAt: { type: Date, required: true },
    updatedAt: { type: Date, required: true }
});