import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

export const VerifySchema = new mongoose.Schema({
    code: { type: String, required: true },
    userRef: { type: String, required: true },
    iat: { type: Date, required: true }
});