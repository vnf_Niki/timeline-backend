import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

export const InviteSchema = new mongoose.Schema({
    code: { type: String, required: true },
    userRef: { type: String, required: true },
    createdAt: { type: Date, required: true }
});