import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

export const GroupSchema = new mongoose.Schema({
    name: { type: String, required: true },
    description: { type: String },
    password: { type: String, required: true },
    titlebar: { type: [{}] },
    usersRefs: { type: [String] },
    postRefs: { type: [String] },
    createdAt: { type: Date, required: true },
    updatedAt: { type: Date, required: true }
});