import { FileInterceptor, FilesInterceptor } from "@nestjs/platform-express";
import { extname } from "path";
import { diskStorage } from 'multer'
import { environment } from "src/environments/environment";
const fs = require('fs')

export const imageFileFilter = async (req, file, callback) => {
  if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
    return await callback(new Error('Only image files are allowed!'), false);
  }
  await callback(null, true);
};

export const editFileName = async (req, file, callback) => {

  let name = file.originalname.split('.')[0];
  const fileExtName = extname(file.originalname);
  const date = new Date().getTime();

  if (req.query.groupId) {
    let filesDir = environment.imageDir.baseDir + '/' + environment.imageDir.postImage + '/' + req.query.groupId;
    if (!fs.existsSync(filesDir)) {
      fs.mkdirSync(filesDir);
    }
    name = req.query.groupId + '/' + name;
  }
  await callback(null, `${name}-${date}${fileExtName}`);
};

export const profileImageUploadFileInterceptor = FileInterceptor('profileImage', { storage: diskStorage({ destination: environment.imageDir.baseDir + '/' + environment.imageDir.profileImage, filename: editFileName }), fileFilter: imageFileFilter });
export const postImageUploadFileInterceptor = FileInterceptor('image', { storage: diskStorage({ destination: environment.imageDir.baseDir + '/' + environment.imageDir.postImage, filename: editFileName }), fileFilter: imageFileFilter });
export const postImagesUploadFileInterceptor = FilesInterceptor('images', environment.maxPostImages, { storage: diskStorage({ destination: environment.imageDir.baseDir + '/' + environment.imageDir.postImage, filename: editFileName }), fileFilter: imageFileFilter });