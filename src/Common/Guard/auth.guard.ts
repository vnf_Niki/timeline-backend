import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import { JWTService } from '../../Common/Service/jwt.service';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private jwtService: JWTService) { }

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest();
    const params = request.headers;
    const authToken = params['auth-token'];
    
    return this.jwtService.verifyToken(authToken);
  }
}
