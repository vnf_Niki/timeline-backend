import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Group } from '../interface/group.interface';
import { JWTService } from '../../Common/Service/jwt.service';

import * as multer from 'multer'

@Injectable()
export class GroupGuard implements CanActivate {
  constructor(
    @InjectModel('Group') private GroupModel: Model<Group>,
    private jwtService: JWTService) { }

  //Needs:
  //Headers ['auth-token']
  //Body groupId || Query groupId

  //Is User in Group
  async canActivate(
    context: ExecutionContext,
  ): Promise<boolean> {
    let request = context.switchToHttp().getRequest();

    let params = request.headers;
    let authToken = params['auth-token'];
    const userToken: any = await this.jwtService.returnUserId(authToken)
    params = request.body;
    let groupId = params.groupId;
    if (!groupId)
      groupId = request.query.groupId;

    try {
      let group: any = <Group>await this.GroupModel.findById(groupId).exec();

      let found = false;
      group.usersRefs.forEach(id => {
        if (id === userToken.id)
          found = true;
      });

      return Promise.resolve(found);
    } catch (error) {
      return Promise.resolve(false);
    }
  }
}