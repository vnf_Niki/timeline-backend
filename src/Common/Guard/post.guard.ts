import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Group } from '../interface/group.interface';
import { Post } from '../interface/post.interface';


@Injectable()
export class PostGuard implements CanActivate {

  constructor(
    @InjectModel('Post') private PostModule: Model<Post>,
    @InjectModel('Group') private GroupModel: Model<Group>) { }

  //Needs:
  //Body postId
  //Body groupId

  //Is Post in Group
  async canActivate(
    context: ExecutionContext,
  ): Promise<boolean> {
    const request = context.switchToHttp().getRequest();
    const params = request.body;
    const groupId = params.groupId;

    let postIds = params.postIds;

    if (!postIds[0])
      return Promise.resolve(false);

    try {
      let group: any = <Group>await this.GroupModel.findById(groupId).exec();

      let count = 0;
      postIds.forEach(postId => {
        group.postRefs.forEach(id => {
          if (id === postId)
            count++;
        });
      });

      //Wenn alle PostIds auch in der Gruppe sind
      if (count === postIds.length)
        return Promise.resolve(true);
      else
        return Promise.resolve(false);

    } catch (error) {
      return Promise.resolve(false);
    }
  }
}
