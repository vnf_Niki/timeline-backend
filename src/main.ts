import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { environment } from './environments/environment';
import { NestExpressApplication } from '@nestjs/platform-express';
import * as rateLimit from 'express-rate-limit';

async function bootstrap() {

  const app = await NestFactory.create<NestExpressApplication>(AppModule, { cors: environment.cors });

  const options = new DocumentBuilder()
    .setTitle(environment.title)
    .setDescription(environment.description)
    .setVersion(environment.version)
    .addTag(environment.tag)
    .build();

  const document = SwaggerModule.createDocument(app, options);

  SwaggerModule.setup(environment.path, app, document);

  app.use(
    rateLimit({
      windowMs: 15 * 60 * 1000, // 15 minutes
      max: 300,
    }),
  );

  app.set('trust proxy', 1)

  await app.listen(3000);
}

bootstrap();