import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './user/user.module';
import { environment } from './environments/environment';
import { MongooseModule } from '@nestjs/mongoose';
import { GroupModule } from './group/group.module';
import { PostModule } from './post/post.module';
import { MulterModule } from '@nestjs/platform-express';
import { StaticModule } from './static/static.module';
import { VerifyModule } from './verify/verify.module';
import { InviteModule } from './invite/invite.module';

@Module({
  imports: [
    MongooseModule.forRoot(
      environment.host !== 'localhost' ?
        'mongodb://' + environment.username + ':' + environment.password + '@' + environment.host + ':' + environment.port :
        'mongodb://' + environment.host + ':' + environment.port,
      {
        dbName: environment.dbName
      }),
    MulterModule.register({ dest: environment.imageDir.baseDir, limits: { fieldSize: 30000000 } }),
    UserModule,
    GroupModule,
    PostModule,
    StaticModule,
    VerifyModule,
    InviteModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }